<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: SI2001
  Date: 31/07/2017
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType = "text/html;charset=UTF-8" language = "java" import = "tutorial.serv.User" %>
<%@ page import = "java.lang.reflect.Parameter" %>
<%@ page import = "java.sql.ResultSet" %>
<%@ page import="tutorial.serv.MaritalStatus" %>
<html>
<head>
    <meta http-equiv = "Content-Type" content = "text/html" charset = "ISO-8859-1">
    <title>Edit</title>
</head>
<body bgcolor = "#FFFF99">
<div class = "container-fluid" align = "center">

<h1><i>You are wrong?..Edit your data!</i></h1>


<form method = "POST" action = "/ModifyServlet.do">
<p>

    First name:   <br><br> <input type = "text" size = "40" maxlength = "40" name = "first_name" value = "${user.firstname}"/><br>
    Last name:    <br><br> <input type = "text" size = "40" maxlength = "40" name = "last_name" value = "${user.lastname}"  /><br>
    Country:      <br><br> <input type = "text" size = "40" maxlength = "40" name = "country" value = "${user.country}"     /><br>
    Date of birth:<br><br> <input type = "date" size = "40" maxlength = "40" name = "date" value = "${user.date}"           /><br><br>
    Marital status:          <select name="marital_status_id">

                    <c:forEach items = "${maritalStatusList}" var = "maritalStatus">
                        <option value = "${maritalStatus.maritalStatusId}" ${maritalStatus.maritalStatusId eq user.maritalStatus.maritalStatusId ? 'selected' : ''}>
                                ${maritalStatus.status}
                        </option>
                    </c:forEach>

</select>  <br><br><br><br>

    <input type = "hidden" name = "id" value = "${user.id}" />

</p>

    <br><br>

    <input type = "SUBMIT" name = "submit" value = "Save">

</form>

    <a href = "index.jsp"><input type = "SUBMIT" name = "submit" value = "Home"></a>


</div>

</body>

</html>
