package tutorial.serv;

import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;


@WebServlet(name = "DeleteServlet", description= "Delete user", urlPatterns = {"/DeleteUser.do"} )
public class DeleteServlet extends HttpServlet {
    private static final long serialVersioUID=1L;
    private ConnectionUtil c = new ConnectionUtil();

    public DeleteServlet(){
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session = ConnectionUtil.goSession();


        int id=Integer.parseInt(request.getParameter("id"));
        User u =session.get(User.class, id);
        if (u!=null){
            session.delete(u);
        }

        ConnectionUtil.closeSession(session);

        RequestDispatcher view= request.getRequestDispatcher("index.jsp"); // ..e lo inviamo alla JSP
        view.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
