package tutorial.serv;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.Connection;
import java.sql.SQLException;

    public class ConnectionUtil {

    private static SessionFactory factory;

private static ConnectionUtil connect = null; //instanziamento pigro

    static  {

        Configuration config = new Configuration().configure();
        config.addAnnotatedClass(User.class);
        ServiceRegistry servReg = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        factory = config.buildSessionFactory(servReg);

        //SessionFactory factory = new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
    }


        public static Session goSession(){
        Session session= factory.openSession();
        session.beginTransaction();

        return session;
    }

        public static void closeSession(Session session){
            session.getTransaction().commit();
            session.close();
        }



}
