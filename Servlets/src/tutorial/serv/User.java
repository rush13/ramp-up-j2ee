package tutorial.serv;

import javax.persistence.*;

@Entity
@Table (name= "USERS",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"first_name","last_name"})})
public class User {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id", nullable=false, unique=true)
    private int id;

    @Column(name="first_name", length=40, nullable=false)
    private String firstname;

    @Column(name="last_name", length=40, nullable=false)
    private String lastname;

    @Column(name="country", length=40, nullable=true)
    private String country;

    @Column(name = "date", length =10, nullable =true)
    private String date;


    public User(String firstname, String lastname, String country, String date) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.country = country;
        this.date = date;
    }

    public User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
