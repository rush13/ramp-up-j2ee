package tutorial.serv;


import org.hibernate.Session;



import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.*;



@WebServlet(name = "TrainingServlet",description = "Create you new servlet", urlPatterns = {"/CreateUser.do"})
public class TrainingServlet extends HttpServlet {

    private static final long serialVersioUID=1L;
    private ConnectionUtil c = new ConnectionUtil();

    public TrainingServlet(){
        super();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        Session session =ConnectionUtil.goSession();

        User u = new User (request.getParameter("first_name"), request.getParameter("last_name"), request.getParameter("country"), request.getParameter("date"));

        session.save(u);
        ConnectionUtil.closeSession(session);


        request.setAttribute("user", u); // inseriamo l'oggetto User nella richiesta..

        RequestDispatcher view= request.getRequestDispatcher("useradd.jsp"); // ..e lo inviamo alla JSP
        view.forward(request, response);

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {





    }
}
