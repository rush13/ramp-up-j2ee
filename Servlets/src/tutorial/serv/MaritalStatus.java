package tutorial.serv;

import javax.persistence.*;


@Entity
    @Table(name = "MARITALSTATUS",
            uniqueConstraints = {@UniqueConstraint(columnNames = {"marital_status_id","status"})})

    public class MaritalStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "marital_status_id", nullable = false, unique = true)
    private Integer maritalStatusId;

    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public MaritalStatus (String status){
        this.status=status;

    }

    public MaritalStatus(){

    }

    public String getStatus() {
        return status;
    }

    public Integer getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(Integer maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
