package tutorial.serv;

import org.hibernate.Session;


import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "modify", description = "modify user", urlPatterns = { "/ModifyServlet.do"})
public class ModifyServlet extends HttpServlet {

        private static final long serialVersioUID=1L;

        public ModifyServlet(){
            super();
        }

            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

                Session session = ConnectionUtil.openSession();
                User user = new User();
            if (request.getParameter("id") != null &&!request.getParameter("id").equals("")){

                Integer userId = Integer.parseInt(request.getParameter("id"));
                user = session.get(User.class, userId);

            }

                String sql = "FROM MaritalStatus";
                Query query = session.createQuery(sql);
                List<MaritalStatus> maritalStatusList = query.getResultList();



                ConnectionUtil.closeSession(session);



                request.setAttribute("maritalStatusList", maritalStatusList);
                request.setAttribute("user", user);

                RequestDispatcher view = request.getRequestDispatcher("modifyuser.jsp"); // ..e lo inviamo alla JSP
                view.forward(request, response);


            }

            protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

                Session session = ConnectionUtil.openSession();

                int maritalStatusId = Integer.parseInt(request.getParameter("marital_status_id"));
                String nome = request.getParameter("first_name");
                String cognome = request.getParameter("last_name");
                String paese = request.getParameter("country");
                String datanascita = request.getParameter("date");
                String ID = request.getParameter("id");
                //int id = Integer.parseInt(ID);

                User user = new User();
                if (request.getParameter("id") != null &&!request.getParameter("id").equals("")){

                    Integer userId = Integer.parseInt(request.getParameter("id"));
                    user.setId(userId);
                }

                    user.setFirstname(nome);
                    user.setLastname(cognome);
                    user.setCountry(paese);
                    user.setDate(datanascita);
                    user.setMaritalStatus(session.get(MaritalStatus.class, maritalStatusId));
                    session.saveOrUpdate(user);


                ConnectionUtil.closeSession(session);
                response.sendRedirect("/Search.do");

            }
}
