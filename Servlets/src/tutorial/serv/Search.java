package tutorial.serv;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.criterion.Restrictions.or;
@WebServlet(name = "Search", description = "Search String", urlPatterns = { "/Search.do"})
public class Search extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Session session = ConnectionUtil.openSession();

        CriteriaBuilder criteria = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteria.createQuery(User.class);
        Root<User> rootUser = criteriaQuery.from(User.class);
        criteriaQuery.select(rootUser);
        List<User> users = session.createQuery(criteriaQuery).getResultList();

        ConnectionUtil.closeSession(session);

        request.setAttribute("users", users);
        RequestDispatcher view = request.getRequestDispatcher("printlist.jsp");
        view.forward(request, response);


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        Session session = ConnectionUtil.openSession();


        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        List<User> users = new ArrayList<>();

        String query = request.getParameter("query");

        criteriaQuery
                .select(userRoot);

        if (query != null && !query.equals("")) {
            String param = "%" + query + "%";
            criteriaQuery
                    .where(
                            criteriaBuilder.or(
                                    criteriaBuilder.like(userRoot.get("firstname"), param),
                                    criteriaBuilder.like(userRoot.get("lastname"), param),
                                    criteriaBuilder.like(userRoot.get("country"), param),
                                    criteriaBuilder.like(userRoot.get("date"), param)


                            )
                    );

            users = session.createQuery(criteriaQuery).getResultList();


        }
            request.setAttribute("users", users);


            ConnectionUtil.closeSession(session);
            RequestDispatcher view = request.getRequestDispatcher("printlist.jsp");
            view.forward(request, response);/*response.sendRedirect("printlist.jsp");*/
    }

    }

